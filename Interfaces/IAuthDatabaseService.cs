using System.Data.SqlClient;
using library.Models;

public interface IAuthDatabaseService {
    public void Login(DatabaseService dbService,UserModel user);
    public int Register(DatabaseService dbService,UserModel user);
}