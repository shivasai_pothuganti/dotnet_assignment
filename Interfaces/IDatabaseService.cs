using System.Data.SqlClient;

interface IDatabaseService {
    
    public SqlConnection getConnection();

}