using library.Models;
using System.Data.SqlClient;
public interface IAdminDataBaseServices {
    public void AddBook(DatabaseService service,BookModel book);
    public void DeleteBook(DatabaseService service,int id);

    public List<BookModel> GetAllBooks(DatabaseService service);
}

