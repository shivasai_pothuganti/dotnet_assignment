using library.Models;
using System.Data.SqlClient;

public interface ILibraryDataBaseService{
    public void lendBook(UserLendBook credentials,DatabaseService service);
    public void returnBook (int bookTransactionId,DatabaseService service);
}