using library.Models;
using System.Data.SqlClient;
public interface ITransactionDatabaseService {
    public List<BookTransactionsModel> getUserTransactions(int userId,SqlConnection con);
    public List<BookTransactionsModel> getAllTransactions(SqlConnection con);
}