using System.Data.SqlClient;

public class AuthDatabaseService : IAuthDatabaseService{
    private SqlConnection con;
    public void Login(DatabaseService dbService,UserModel user){
        try{
            this.con = dbService.getConnection();
            con.Open();
            try{
                string findUserQuery = "SELECT userId from Users WHERE userId = @userid and password=@password";
                SqlCommand findUsercmd = new SqlCommand(findUserQuery,con);
                findUsercmd.Parameters.AddWithValue("@userid",user.userId);
                findUsercmd.Parameters.AddWithValue("@password",user.password);
                SqlDataReader userReader = findUsercmd.ExecuteReader();
                if(userReader.Read()){
                    Console.WriteLine("userfound");
                }
                else{
                    Console.WriteLine("user not found");
                }
            }
            catch(Exception findUserException){
                Console.WriteLine("Error while searching for the user");
            }
            con.Close();
        }
        catch(Exception connectionFailed){
            Console.WriteLine("connection failed");
        }
    }
    public int Register(DatabaseService dbService,UserModel user){
        this.con = dbService.getConnection();
        try{
            con.Open();
            int userId = -1;
            SqlCommand registeruser = new SqlCommand("INSERT INTO Users(userName,password) OUTPUT Users.userId VALUES(@username,@password)");
            registeruser.Parameters.AddWithValue("@username",user.userName);
            registeruser.Parameters.AddWithValue("@password",user.password);

            try{
                userId = (int)registeruser.ExecuteScalar();
            }
            catch(Exception registerError){
                Console.WriteLine("register error");
            }

            return userId;

            con.Close();
        }   
        catch(Exception connectionFailed){
            Console.WriteLine("connection failed");
        }
        return 0;
    }
}