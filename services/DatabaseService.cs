using System.Data.SqlClient;

public class DatabaseService : IDatabaseService{
    public SqlConnection? con {get;set;}

    public SqlConnection getConnection(){
        con = new SqlConnection(@"Server=localhost\SQLEXPRESS;Database=library;Trusted_Connection=true");
        return con;
    }

}
