using System.Data.SqlClient;
using library.Models;

public class AdminDataBaseService : IAdminDataBaseServices {

    private List<BookModel>? allBooks{get;set;}

    private SqlConnection? con;

    public void AddBook(DatabaseService service,BookModel newBook){
        try{
            this.con = service.getConnection();
            con.Open();
            string addBookQuery = "INSERT INTO Books(bookName,bookAuthor,noOfCopies) OUTPUT INSERTED.bookId VALUES (@bookName,@bookAuthor,@noOfCopies)";
            SqlCommand insertBook = new SqlCommand(addBookQuery,con);
            insertBook.Parameters.AddWithValue("@bookName",newBook.bookName);
            insertBook.Parameters.AddWithValue("@bookAuthor",newBook.bookAuthor);
            insertBook.Parameters.AddWithValue("@noOfCopies",newBook.noOfCopies);
            try{
                int bookId = (int) insertBook.ExecuteScalar();
                SqlCommand addCopies = new SqlCommand("INSERT INTO BooksAvailable VALUES(@bookId,@copiesAvailable)",con);
                addCopies.Parameters.AddWithValue("@bookId",bookId);
                addCopies.Parameters.AddWithValue("@copiesAvailable",newBook.noOfCopies);
                try{
                    addCopies.ExecuteNonQuery();
                    Console.WriteLine("ADDED INTO BOOKS AVAILABLE");
                }
                catch(Exception addCopiesFailed){
                    Console.WriteLine("copies are not added");

                }
            }
            catch(Exception insertionFailed){
                Console.WriteLine("insertion failed");
            }
            con.Close();
        }
        catch(Exception e){
            Console.WriteLine("connection failed");
        }
    }

    public void DeleteBook(DatabaseService service,int bookId){
        this.con = service.getConnection();
        try{
            con.Open();
            string deleteBook = "DELTE FROM Books WHERE bookId = @bookId";
            SqlCommand deleteBookCmd = new SqlCommand(deleteBook,con);
            deleteBookCmd.Parameters.AddWithValue("@bookId",bookId);
            try{
                int res = deleteBookCmd.ExecuteNonQuery();
                try{
                    SqlCommand deleteCopies = new SqlCommand("DELTE FROM BooksAvailable WHERE bookId = @bookid");
                    deleteCopies.Parameters.AddWithValue("@bookid",bookId);
                    int confirmation = deleteCopies.ExecuteNonQuery();
                    Console.WriteLine(confirmation);
                }
                catch(Exception deleteCopies){
                    Console.WriteLine(deleteCopies);
                }
            }
            catch(Exception deleteExecption){
                Console.WriteLine("delete exception");
            }
            con.Close();
        }
        catch(Exception connectionFailed){
            Console.WriteLine("connection failed");
        }
    }

    public List<BookModel> GetAllBooks(DatabaseService service){
        this.con = con;
        try{
            con.Open();
            string getAllBooksQuery = "SELECT * FROM Books";
            SqlCommand getAllBookscmd = new SqlCommand(getAllBooksQuery,con);
            try{
                SqlDataReader getAllBooksReader = getAllBookscmd.ExecuteReader();
                while(getAllBooksReader.Read()){
                    BookModel book = new BookModel((int)getAllBooksReader["bookId"],getAllBooksReader["bookName"].ToString(),getAllBooksReader["bookAuthor"].ToString(),(int)getAllBooksReader["noOfCopies"]);
                    allBooks.Add(book);
                }
            }
            catch(Exception dataReaderFailed){
                Console.WriteLine("data is not fetched");
            }
            con.Close();
        }
        catch(Exception connectionFailed){
            Console.WriteLine("connection failed");
        }
        return allBooks;
    }


}

