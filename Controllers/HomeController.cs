﻿using System.Diagnostics;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;
using library.Models;

namespace library.Controllers;

public class HomeController : Controller
{
    SqlConnection con;
    private readonly ILogger<HomeController> _logger;

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public IActionResult Index(DatabaseService service)
    {
        
        Console.WriteLine("request recied");
        this.con = service.getConnection();
        try{
            con.Open();
            Console.WriteLine("connection succesfull");
        }
        catch(Exception e){
            Console.WriteLine(e);
        }
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
