using Microsoft.AspNetCore.Mvc;

public class AuthController: Controller{
    public IActionResult AdminLogin(){
        return View();
    }
    public IActionResult UserLogin(){
        return View();
    }
    public IActionResult Register(){
        return View();
    }
    public IActionResult Logout(){
        return RedirectToAction("UserLogin");
    }
}